FROM ubuntu:16.04

ARG version=prod

RUN export DEBIAN_FRONTEND=noninteractive \
    && sed -i 's,http://archive.ubuntu.com/ubuntu/,mirror://mirrors.ubuntu.com/mirrors.txt,' /etc/apt/sources.list \
    && apt-get update -qq && apt-get upgrade -qq \
    && apt-get install -y --no-install-recommends \
        python3 \
        python3-pip \
        python3-setuptools \
    && BUILD_DEPS='build-essential python3-dev' \
    && apt-get install -y --no-install-recommends apt-utils ${BUILD_DEPS}

COPY requirements.txt /opt/direct_api/
RUN pip3 install --upgrade setuptools
RUN pip3 install --no-cache-dir -r /opt/direct_api/requirements.txt

COPY api_main.py /opt/direct_api/api_main.py
COPY test_api.py /opt/direct_api/test_api.py

WORKDIR /opt/direct_api/
