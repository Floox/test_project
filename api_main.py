import asyncio
from aiohttp import web


async def myfunc(request):
    print(request)
    web.Response(body=json.dumps(result).encode('utf8'), content_type='application/json')

def app(loop=asyncio.get_event_loop()):
    myapp = web.Application(loop=loop)
    myapp.router.add_route('GET', '/test_route', myfunc)
    return myapp
    
async def shutdown(app, server, handler):
    server.close()
    await server.wait_closed()
    await app.shutdown()
    await handler.finish_connections(1.0)
    await app.cleanup()

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    the_app = app(loop)
    handler = the_app.make_handler()
    foo = loop.create_server(handler, '127.0.0.1', 8090)
    foo = loop.run_until_complete(foo)
    try:
        loop.run_forever()
    except:
        loop.run_until_complete(shutdown(the_app, foo, handler))
        loop.close()
